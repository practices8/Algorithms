const assert = require('assert');
const Queue = require('../Data structure/Queue');

describe("Queue enqueue", () => {
    const queue = new Queue();
    queue.enqueue(1);
    let size = queue.enqueue(1);
    assert.equal(size, 2);
    size = queue.dequeue();
    assert.equal(size, 1);
})
