class Node {
    constructor(val) {
        this.val = val;
        this.next = null;
    }
}

class SinglyLinkedList {
    constructor() {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }

    push(val) {
        console.log("********PUSH*********")

        const newNode = new Node(val);
        if (!this.head) {
            this.head = newNode;
            this.tail = newNode;
        } else {
            this.tail.next = newNode;
            this.tail = newNode;
        }
        this.length++;
        return this;
    }

    traverse() {
        console.log("********TRAVERSE*********")

        let current = this.head;
        while (current) {
            console.log(current.val);
            current = current.next;
        }
    }

    pop() {
        console.log("********POP*********")
        if (!this.head)
            return
        let current = this.head
        let newTail = current;

        while (current.next) {
            newTail = current;
            current = current.next;
        }

        newTail.next = null;
        current = null;

        this.length--;
        if (this.length === 0) {
            this.head = null;
            this.tail = null;
        }
    }

    shift() {
        console.log("********SHIFT*********")

        if (!this.head) {
            return undefined;
        }
        const current = this.head;
        this.head = current.next;
        this.length--;
        return current
    }

    unshift(val) {
        if (!this.head) {
            return this.push(val);
        }
        const newNode = new Node(val);
        newNode.next = this.head;
        this.head = newNode;

        this.length++;
        return this
    }

    get(index) {
        if (index < 0 || index >= this.length)
            return null;
        let count = 0;
        let current = this.head;

        while (count !== index) {
            current = current.next
            count++;
        }
        return current;
    }

    set(index, val) {
        const current = this.get(index);
        if (!current) return false;
        current.val = val;
        return true;
    }

    insert(index, val) {
        if (index < 0 || index > this.length) return false;
        if (index === 0) return this.unshift(val);
        if (index === this.length - 1) return this.push(val);
        const current = this.get(index - 1);
        const newNode = new Node(val);
        newNode.next = current.next;
        current.next = newNode;
        this.length++;
        return this;
    }

    remove(index) {
        if (index < 0 || index >= this.length) return false;
        if (index === this.length - 1) return this.pop();
        if (index === 0) return this.shift();

        const prevNode = this.get(index - 1);
        prevNode.next = prevNode.next.next;
        this.length--;
        return this;
    }

    reverse() {
        console.log("********REVERSE*********")
        let current = this.head;
        this.head = this.tail;
        this.tail = current;
        let prev = null;
        let next;
        for (let i = 0; i < this.length; i++) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        return this;
    }
}

let list = new SinglyLinkedList()

list.push("Man")
list.push("Kim")
list.push("A?");
list.push("Kim")


list.traverse();

list.reverse();
list.traverse();

// list.insert(2, "Ham");
// list.set(0, "Men");
// list .unshift(  "Qoyil1" )
// ;
// list   .unshift(    "Qoyil2"  )
// ;
// list.traverse();

// list.pop();
// list.pop();
// list.traverse();
