class Node {
    constructor(val) {
        this.val = val;
        this.next = null;
        this.prev = null;
    }
}

class DoublyLinkedList {
    constructor() {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }

    push(val) {
        let newNode = new Node(val);
        if (this.length === 0) {
            this.head = newNode;
            this.tail = newNode;
        } else {
            this.tail.next = newNode;
            newNode.prev = this.tail;
            this.tail = newNode;
        }
        this.length++;
        return this;
    }

    pop() {
        let poppedNode = this.tail;
        if (!this.length)
            return undefined;

        if (this.length === 1) {
            this.head = null;
            this.tail = null;
        } else {
            this.tail = poppedNode.prev;
            this.tail.next = null;
            poppedNode.prev = null;
        }
        this.length--
        return poppedNode;
    }

    travese() {
        console.log("*******TRAVERSE**********")
        let current = this.head;

        while (current) {
            console.log(current.val);
            current = current.next
        }
    }

    shift() {
        if (this.length === 0)
            return undefined;
        if (this.length === 1) {
            this.head = null;
            this.tail = null
        } else {
            let currentNode = this.head.next;
            this.head = currentNode;
            currentNode.next = null;
            currentNode.prev = null;
        }

        this.length--
        return this;
    }

    unshift(val) {
        let newNode = new Node(val);
        newNode.prev = null;

        if (this.length === 0) {
            this.head = newNode;
            this.tail = newNode;
        } else {
            newNode.next = this.head;
            this.head.prev = newNode;
            this.head = newNode;
        }
        this.length++;
        return this;

    }

    get(index) {
        if (this.length === 0)
            return null;

        let count = 0;
        let current = this.head;

        while (current) {
            if (count === index) {
                return current;
            }
            current = current.next;
            count++;
        }
        return -1;
    }

    set(index, val) {
        const current = this.get(index);
        if (typeof current === 'number' || !current) return;

        if (current) {
            current.val = val;
        }
    }

    remove(index) {
        if(index>=this.length|| index<0) return false;
        let removedNode = this.get(index)
        if (typeof removedNode === 'number' || !removedNode) return;

        if (index === 0) return this.shift();
        if (index === this.length-1) return this.pop();

        let afterNode = removedNode.next;
        let beforeNode = removedNode.prev;

        beforeNode.next=afterNode;
        afterNode.prev=beforeNode;
        removedNode.next=null;
        removedNode.prev=null;

        this.length--
        return removedNode ;
    }

    insert(index, val) {
        if (index > this.length || index <= 0)
            return null;

        if (this.length === index) {
            return this.push(val);
        }
        if (index === 0) {
            return this.unshift(val)
        }

        const beforeNode = this.get(index - 1);
        const afterNode = beforeNode.next;

        if (beforeNode) {
            const newNode = new Node(val);
            beforeNode.next = newNode;
            newNode.prev = beforeNode;

            newNode.next = afterNode
            afterNode.prev = newNode;
        }

        this.length++;
        return this;
    }
}

// 1<--->2<--->3
//        c
//  newNode;

const list = new DoublyLinkedList();
list.push(2)
list.push(4);
list.push(9);
list.push(5);
// list.insert(2, 6)
// list.set(5, 3)

list.travese();
