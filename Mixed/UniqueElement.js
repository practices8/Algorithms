

function uniqueElement(arr){

    let current=0, unique=0;

    while(arr.length>current){
        if(arr[current]>arr[unique]){
            unique++;
            [arr[current], arr[unique]]=[arr[unique],arr[current]];
        }
        current++;
    }
    return unique
}

const a=[1,1,3,3,5,5,6,7,8,9,9,10];
uniqueElement(a);
console.log(uniqueElement(a));

for (let i = 0; i <=uniqueElement(a); i++) {
    console.log(a[i]);
}
