function BinarySearch(arr, val) {
    var start = 0;
    var end = arr.length - 1;
    var middle = Math.floor((start + end) / 2);

    while (arr[middle] !== val && start <= end) {
        if (arr[middle] < val) start = middle + 1;
        else if (arr[middle] > val) end = middle - 1;
        else return middle;
        middle = Math.floor((start + end) / 2);
    }

    if (arr[middle] === val) return middle;
    return -1;
}


module.exports = BinarySearch;
