 Array.prototype.bubbleSort=BubbleSort;

function BubbleSort(arr){
    let noSwaps;
    if(!arr) arr=this;
    console.time()
    for (let i=arr.length; i> 0;i--){
        noSwaps=true;
        for (let j = 0; j < i-1 ; j++) {

            if(arr[j]>arr[j+1]){
                [arr[j], arr[j+1]]=[arr[j+1], arr[j]]
                noSwaps=false;
            }
        }
        if (noSwaps) break;
    }
    console.timeEnd()
    return arr;
}
