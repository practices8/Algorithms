function merge(arr1, arr2) {
    let right = 0;
    let left = 0;
    let results = []
    while (left < arr1.length && right < arr2.length) {
        if (arr1[left] < arr2[right]) {
            results.push(arr1[left]);
            left++;
        } else {
            results.push(arr2[right]);
            right++;
        }
    }

    while (left < arr1.length) {
        results.push(arr1[left]);
        left++
    }
    while (right < arr2.length) {
        results.push(arr2[right]);
        right++
    }

    return results;
}

function MergeSort(arr) {
    if (arr.length <= 1) return arr;
    let mid = Math.floor(arr.length / 2);
    const left = MergeSort(arr.slice(0, mid));
    const right = MergeSort(arr.slice(mid));
    return merge(left, right);
}


Array.prototype.mergeSort =  MergeSort;
