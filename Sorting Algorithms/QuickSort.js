function pivot(arr, start = 0, end = arr.length - 1) {

    const swap = (arr, indx1, indx2) => {
        [arr[indx1], arr[indx2]] = [arr[indx2], arr[indx1]];
    }

    let pivot = arr[start];
    let swapInx = start;
    for (let i = start + 1; i <= end; i++) {
        if (pivot > arr[i]) {
            swapInx++;
            swap(arr, swapInx, i);
        }
    }

    swap(arr, start, swapInx);
    return swapInx;
}


function QuickSort(arr, left=0, right=arr.length-1){
    if(left<right){
        let pivotIndx = pivot(arr, left,right);
        QuickSort(arr, left, pivotIndx-1);
        QuickSort(arr, pivotIndx+1, right);
    }
    return arr;
}
Array.prototype.quickSort = QuickSort;
