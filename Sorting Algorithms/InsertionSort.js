Array.prototype.insertionSort = InsertionSort;

function InsertionSort(arr) {
    if (!arr) arr = this;
    for (let i = 1; i < arr.length; i++) {
        let currentValue = arr[i];
        let currentIndex = i;
        for (let j = i - 1; j >= 0 && arr[j] > currentValue; j--) {
            arr[j + 1] = arr[j];
            currentIndex = j;
        }
        arr[currentIndex] = currentValue;
    }
    return arr;
}

// 4,2,3,5,1,6
// 2,4,3,5,1,6
// 2,3,4,5,1,6
// 123456
